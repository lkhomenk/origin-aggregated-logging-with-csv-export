FROM docker.io/openshift/origin-logging-kibana5:v3.11.0


RUN cd ${KIBANA_HOME} && \
  wget https://github.com/fbaligand/kibana/releases/download/v5.6.13-csv-export/csv-export-patch.tar.gz && \
  tar -zxvf csv-export-patch.tar.gz && \
  rm -f csv-export-patch.tar.gz
